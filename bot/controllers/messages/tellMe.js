const { messageTypes } = require('../../../helpers/enums');

async function canHandle(text) {
  const result = text == 'tell me my id';
  console.debug('[skill][tell-me] canHandle:', result);
  return result;
}

async function handler({ userChatId }) {
  return {
    toStart: true,
    message: {
      type: messageTypes.simpleMessage,
      text: `Your chatId is ${userChatId}`,
    }
  }
}

module.exports = {
  canHandle,
  handler,
};
