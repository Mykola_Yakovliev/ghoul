const { messageType } = require('../../../helpers/enums');

async function canHandle(text) {
  const result = text == 'Start';
  console.debug('[skill][to-start] canHandle:', result);
  return result;
}

async function handler() {
  return { toStart: true };
}

module.exports = {
  canHandle,
  handler,
};
