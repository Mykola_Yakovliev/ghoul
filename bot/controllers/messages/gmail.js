const { messageTypes } = require('../../../helpers/enums');
const { generateAuthUrl } = require('../../../skills/gmail');

async function canHandle(text) {
  const result = text == 'Enable gmail';
  console.debug('[skill][gmail] canHandle:', result);
  return result;
}

async function handler() {
  const link = generateAuthUrl();
  return {
    toStart: false,
    message: {
      type: messageTypes.simpleMessage,
      text: `Please open link and copy access token\n${link}`,
    }
  }
}

module.exports = {
  canHandle,
  handler,
};
