const { util, PromiseProgerss } = require('../../../helpers');
const { callbackQueryType, messageTypes, cardStatus, responderStatus } = require('../../helpers/enums');

module.exports = {
  action: {
    [callbackQueryType.status.card.choose]: ({ userChatId, value }) => {
      return new PromiseProgerss(async (resolve, reject, progress) => {
        if (cardStatus.accepted == value) {
          const result = await util.request({
            isInternal: true,
            method: 'POST',
            body: {
              userChatId,
              Responder: {
                status: responderStatus.busy
              }
            }
          });
          progress({
            type: messageTypes.simpleMessage,
            chat_id: userChatId,
            text: result.statusCode == 200 ? `Ваш статус був змінений на ${responderStatus.busy}` : 'Ми не змогли оновити ваш статус'
          });
        }
        const result = await util.request({ isInternal: true, method: 'POST', body: { userChatId, Responder: { status: value } } })
        resolve({
          toStart: false,
          message: {
            type: messageTypes.simpleMessage,
            chat_id: userChatId,
            text: result.statusCode == 200 ? `Статус виклику був змінений на ${value}` : 'Ми не змогли оновити ваш статус виклику'
          }
        });
      })
    }
  }
}