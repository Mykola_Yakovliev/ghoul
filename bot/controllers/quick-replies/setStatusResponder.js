const { util, PromiseProgerss } = require('../../../helpers');
const { callbackQueryType, messageTypes, responderStatus } = require('../../helpers/enums');

function generateChooseStatus() {
  return {
    type: messageTypes.withKeyboard,
    text: 'Оберіть ваш статус',
    reply_markup: JSON.stringify({
      inline_keyboard: [[
        { text: responderStatus.ready, callback_data: `${callbackQueryType.status.responder.choose}_${responderStatus.ready}` },
        { text: responderStatus.busy, callback_data: `${callbackQueryType.status.responder.choose}_${responderStatus.busy}` },
        { text: responderStatus.offduty, callback_data: `${callbackQueryType.status.responder.choose}_${responderStatus.offduty}` }
      ]]
    })
  }
}

module.exports = {
  action: {
    [callbackQueryType.status.responder.set]: () => new PromiseProgerss(async (resolve) =>
      resolve({
        toStart: false,
        message: generateChooseStatus()
      })
    ),
    [callbackQueryType.status.responder.choose]: ({ userChatId, value }) =>
      new PromiseProgerss(async (resolve) => {
        const result = await util.request({ isInternal: true, method: 'POST', body: { userChatId, Responder: { status: value } } })
        resolve({
          toStart: true,
          message: {
            type: messageTypes.simpleMessage,
            chat_id: userChatId,
            text: result.statusCode == 200 ? `Ваш статус був змінений на ${value}` : 'Ми не змогли оновити ваш статус'
          }
        });
      })
  }
}