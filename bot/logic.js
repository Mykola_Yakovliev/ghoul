const Telegram = require('telegram-bot-api');
const controllers = require('./controllers');
const { util: { request } } = require('../helpers');
const {
  messageTypes,
  telegramMessageTypes,
  telegramMessageHandlers,
  callbackQueryType,
} = require('../helpers/enums');


class TelegramBot {

  static detectMessageType(body) {
    for (const type in telegramMessageTypes)
      if (type in body)
        return telegramMessageTypes[type];
    return null;
  }

  constructor(token) {
    this.token = token;
    this.api = new Telegram({ token: this.token });
    this.controllers = controllers;
  }

  afterMessage(userChatId) {
    this.api.sendMessage({
      chat_id: userChatId,
      text: 'Це базове повідомлення, яке з\'являється, коли всі і виконані',
      reply_markup: JSON.stringify({
        inline_keyboard: [
          [{ text: 'Всановити мій статус реагувальника', callback_data: callbackQueryType.status.responder.set },]
        ]
      })
    })
  }

  async onMessage(handler, pack) {
    const userChatId = handler.getUserChatId(pack);
    const text = handler.getText(pack);
    let controller;
    for (const contr of this.controllers.message) {
      const result = await contr.canHandle(text);
      if (result) {
        controller = contr;
        break;
      }
    }
    if (!controller) {
      return await this.canNotHandle(userChatId);
    }
    const result = await controller.handler({ userChatId, text });
    if (!result) {
      return await this.canNotController(userChatId);
    }
    await this.sendMessage(userChatId, result.message);
    return { toStart: result.toStart, userChatId };
  }

  async onQuickReplies(handler, pack) {
    const userChatId = handler.getUserChatId(pack);
    const [query, value] = handler.getQuery(pack).split('_');
    let controller;
    for (const contr of this.controllers.callbackQuery) {
      if (contr.action[query]) {
        controller = contr.action[query];
        break;
      }
    }
    if (!controller) {
      return await this.canNotHandle(userChatId);
    }
    if (controller.progress)
      controller.progress((message) => {
        this.sendMessage(userChatId, message)
      });
    const result = await controller({ userChatId, value })
      .progress((message) => {
        this.sendMessage(userChatId, message)
      });

    if (!result) {
      return await this.canNotController(userChatId);
    }
    await this.sendMessage(userChatId, result.message);
    return { toStart: result.toStart, userChatId };
  }

  async haveMessage(body) {
    const telegramType = TelegramBot.detectMessageType(body);
    if (!telegramType) {
      console.error('[error][method:haveMessage] can not detec message type from telegram', JSON.stringify(body));
      return;
    }
    const telegramHandler = telegramMessageHandlers[telegramType];
    switch (telegramType) {
      case telegramMessageTypes.message: {
        this.onMessage(telegramHandler, body)
          .then(({ userChatId, toStart }) => toStart && this.afterMessage(userChatId))
          .catch(err => console.error(err));
        break;
      }
      case telegramMessageTypes.callback_query: {
        this.onQuickReplies(telegramHandler, body)
          .then(({ userChatId, toStart }) => toStart && this.afterMessage(userChatId))
          .catch(err => console.error(err));
        break;
      }
    }

  }
  sendMessage(userChatId, pack) {
    if (!pack) {
      return null;
    }
    const { type } = pack;
    switch (type) {
      case messageTypes.withKeyboard:
      case messageTypes.simpleMessage: {
        return this.api.sendMessage({ ...pack, chat_id: userChatId });
      }
      case messageTypes.location: {
        this.api.sendLocation({ ...pack, chat_id: userChatId });
        break;
      }
      default: {
        console.error('[bot][mehod:sendMessage]Undefined type:', type);
      }
    }

  }
  async canNotHandle(userChatId) {
    await this.api.sendMessage({ chat_id: userChatId, text: 'Not found controller' });
    return userChatId;
  }
  async canNotController(userChatId) {
    await this.api.sendMessage({ chat_id: userChatId, text: 'Controller not return value' });
    return userChatId;
  }
}

module.exports = { TelegramBot };
