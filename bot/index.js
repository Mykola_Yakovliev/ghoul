const assert = require('assert');
const { checkWebhook, setWebhook, verification } = require('./helper');
const getRouter = require('./router');
const { TelegramBot } = require('./logic');

async function setup(app, { token, webhook }) {
  assert(token != null);
  assert(webhook != null);
  console.log('webhook', webhook)
  const telegramBot = new TelegramBot(token);
  app.use(getRouter(telegramBot));

  const result = {};
  result.verification = await verification(token);
  console.debug(result.verification);
  if (!result.verification) {
    throw new Error(`Telegram bot with token: ${token} is not verify`);
  }
  result.setWebhook = await checkWebhook(token)
    .then(res => {
      console.debug(res);
      const { result: { url: oldUrl } } = res;
      if (oldUrl != webhook) {
        return setWebhook({ token, url: webhook });
      }
    });
  console.debug(result.setWebhook);

  return { telegramBot };
}

module.exports = { setup }