const mongoose = require('mongoose');
const { Schema } = mongoose;

const schema = new Schema({
  telegram: { chatId: { type: String, required: true, unique: true, index: true } },
  gmail: Object,
});

module.exports = mongoose.models['users'] || mongoose.model('users', schema, 'users');