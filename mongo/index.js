const mongoose = require('mongoose');

module.exports.connectToMongo = async function (
  uri = process.env.MONGODB_URI,
  options = { ssl: true, poolSize: 2, useNewUrlParser: false },
) {
  console.debug(`[mongo][connect] db name ${process.env.DATABASE_NAME} mongo url ${uri}`);
  await mongoose.connect(uri, options);
}