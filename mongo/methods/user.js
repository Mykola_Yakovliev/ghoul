const { user } = require('../model');


module.exports.find = chatId => user.findOne({ telegram: { chatId } }).exec();
