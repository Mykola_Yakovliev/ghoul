const express = require('express');
const bodyParser = require('body-parser');
const { setEnv, eventListener } = require('./helpers');

setEnv(process.env.envPath);

const { logger } = require('./helpers/middleware');
const { serverEvent } = require('./helpers/enums');
const { connectToMongo } = require('./mongo');
const { setup } = require('./bot');
const APIRouter = require('./api');


process.on('uncaughtException', (err) => {
  console.log('[uncaughtException]', JSON.stringify(err), err);
  process.exit(0);
});

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(logger);
app.use(express.static('public'));
eventListener(app);

app.listen(process.env.port, async () => app.emit(serverEvent.serverOnline));
connectToMongo()
  .then(() => {
    app.emit(serverEvent.mongo.connected);
    return setup(app, { token: process.env.telegramBotToken, webhook: `${process.env.HOST_URL}/telegram` })
  })
  .then(bot => {
    app.use(APIRouter(bot.telegramBot));
    app.emit(serverEvent.serviceOnline);
  })
  .catch(err => {
    console.error(err);
  })

module.exports = app;

