const express = require('express');
const router = express.Router();

const { secretValidation } = require('../helpers/middleware');
const createCard = require('./create-card');

module.exports = function (bot) {
  router.post('*', secretValidation);

  router.post('/cards', createCard(bot));

  return router;
}