/* eslint-disable no-template-curly-in-string */

module.exports = function (path) {
  const options = { debug: false, path };
  const result = require('dotenv').config(options);

  if (result.error) {
    throw result.error;
  } else {
    for (const key in result.parsed) {
      process.env[key] = result.parsed[key];
      console.debug(`[envs][event:find] key:${key}, value:${result.parsed[key].length > 40 ? `${result.parsed[key].substring(0, 40)}...` : result.parsed[key]}`)
    }

    const customFields = process.env.customFields.split(' ');
    customFields.forEach(field => {
      process.env[field] = process.env[`${field}_${process.env.ENV}`];
      console.log(`[envs][event:set] ${field}:${process.env[field]}`);
    });
    process.env.MONGODB_URI = process.env.MONGODB_URI.replace('{{dbName}}', process.env.DATABASE_NAME);
  }
}
