module.exports.serverEvent = {
  serverOnline: 'server online',
  serviceOnline: 'service online',
  mongo: {
    connected: 'mongo connected',
    error: 'mongo error',
  },
};

module.exports.messageTypes = {
  simpleMessage: 'simpleMessage',
  withKeyboard: 'withKeyboard',
  location: 'location'
};

module.exports.callbackQueryType = {
  status: {
    responder: {
      set: 'set responder status',
      choose: 'choose responder status'
    },
    card: {
      choose: 'choose card status',
      showOnMap: 'card show on map',
    }
  },
};

module.exports.telegramMessageTypes = {
  message: 'message',
  callback_query: 'callback_query'
};

module.exports.telegramMessageHandlers = {
  message: {
    getUserChatId: (pack) => pack.message.from.id,
    getText: (pack) => pack.message.text,
  },
  callback_query: {
    getUserChatId: (pack) => pack.callback_query.from.id,
    getQuery: (pack) => pack.callback_query.data,
  },
}

module.exports.responderStatus = { ready: "Ready", busy: "Busy", offduty: "Offduty" }

module.exports.cardStatus = {
  ready: "Ready",
  sent: "Sent",
  delivered: "Delivered",
  accepted: "Accepted",
  arrived: "Arrived",
  finished: "Finished"
}