const { serverEvent } = require('./enums');


function eventListener(app) {
  app.on(serverEvent.serverOnline, () => {
    console.debug(`[eventListener][event:${serverEvent.serverOnline}]`);
  });
  app.on(serverEvent.serviceOnline, () => {
    console.debug(`[eventListener][event:${serverEvent.serviceOnline}]`);
  });
  app.on(serverEvent.mongo.connected, () => {
    console.debug(`[eventListener][event:${serverEvent.mongo.connected}]`);
  });
  app.on(serverEvent.mongo.error, (err) => {
    console.error(`[error][eventListener][event:${serverEvent.mongo.error}]`, err);
  });
}

module.exports = { eventListener };
